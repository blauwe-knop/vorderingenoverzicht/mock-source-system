// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import (
	"fmt"
	"sort"
	"time"
)

type ContactOptie struct {
	ContactVorm  ContactVorm `json:"contact_vorm"`
	Url          string      `json:"url"`
	Naam         string      `json:"naam"`
	Omschrijving string      `json:"omschrijving"`
	Prioriteit   int         `json:"prioriteit"`
}

type FinancieleVerplichtingType string

type FinancialClaimsInformationDocumentFinancieleZaak struct {
	Zaakkenmerk               Zaakkenmerk          `json:"zaakkenmerk"`
	Bsn                       Bsn                  `json:"bsn"`
	TotaalFinancieelVerplicht int                  `json:"totaal_financieel_verplicht"`
	TotaalFinancieelVereffend int                  `json:"totaal_financieel_vereffend"`
	Saldo                     int                  `json:"saldo"`
	SaldoDatumtijd            time.Time            `json:"saldo_datumtijd"`
	Gebeurtenissen            []interface{}        `json:"gebeurtenissen"`
	Achterstanden             []GebeurtenisKenmerk `json:"achterstanden"`
	ContactOpties             []ContactOptie       `json:"contact_opties"`
}

type FinancialClaimsInformationDocumentBody struct {
	AangeleverdDoor   string                                               `json:"aangeleverd_door"`
	DocumentDatumtijd time.Time                                            `json:"document_datumtijd"`
	Bsn               Bsn                                                  `json:"bsn"`
	Beschikbaarheid   map[FinancieleVerplichtingType]BeschikbaarheidStatus `json:"beschikbaarheid"`
	FinancieleZaken   []FinancialClaimsInformationDocumentFinancieleZaak   `json:"financiele_zaken,omitempty"`
}

type FinancialClaimsInformationDocument struct {
	Type    string                                 `json:"type" default:"FINANCIAL_CLAIMS_INFORMATION_DOCUMENT"`
	Version string                                 `json:"version" default:"4"`
	Body    FinancialClaimsInformationDocumentBody `json:"body"`
}

func NewFinancieleZaakFromEvents(events []Event, achterstanden []GebeurtenisKenmerk, contactOpties []ContactOptie) (*FinancialClaimsInformationDocumentFinancieleZaak, error) {
	financieleZaak := &FinancialClaimsInformationDocumentFinancieleZaak{}
	financieleZaak.Achterstanden = achterstanden
	financieleZaak.ContactOpties = contactOpties

	sort.SliceStable(events, func(i, j int) bool {
		return events[i].GetBaseEvent().DatumtijdGebeurtenis.Before(events[j].GetBaseEvent().DatumtijdGebeurtenis)
	})

	for _, event := range events {
		err := financieleZaak.On(event)
		if err != nil {
			return nil, err
		}
	}

	return financieleZaak, nil
}

func (financieleZaak *FinancialClaimsInformationDocumentFinancieleZaak) On(event Event) error {
	switch event.GetBaseEvent().GebeurtenisType {
	case "FinancieleVerplichtingOpgelegd":
		financieleVerplichtingOpgelegd, ok := event.(FinancieleVerplichtingOpgelegd)
		if !ok {
			return fmt.Errorf("cast to FinancieleVerplichtingOpgelegd failed")
		}

		financieleZaak.Zaakkenmerk = financieleVerplichtingOpgelegd.Zaakkenmerk
		financieleZaak.Bsn = financieleVerplichtingOpgelegd.Bsn
		financieleZaak.TotaalFinancieelVerplicht += financieleVerplichtingOpgelegd.Bedrag
		financieleZaak.SaldoDatumtijd = financieleVerplichtingOpgelegd.DatumtijdGebeurtenis

	case "FinancieleVerplichtingKwijtgescholden":
		financieleVerplichtingKwijtgescholden, ok := event.(FinancieleVerplichtingKwijtgescholden)
		if !ok {
			return fmt.Errorf("cast to FinancieleVerplichtingKwijtgescholden failed")
		}

		financieleZaak.Zaakkenmerk = financieleVerplichtingKwijtgescholden.Zaakkenmerk
		financieleZaak.Bsn = financieleVerplichtingKwijtgescholden.Bsn
		financieleZaak.TotaalFinancieelVerplicht -= financieleVerplichtingKwijtgescholden.BedragKwijtschelding
		financieleZaak.SaldoDatumtijd = financieleVerplichtingKwijtgescholden.DatumtijdGebeurtenis

	case "FinancieleVerplichtingGecorrigeerd":
		financieleVerplichtingGecorrigeerd, ok := event.(FinancieleVerplichtingGecorrigeerd)
		if !ok {
			return fmt.Errorf("cast to FinancieleVerplichtingGecorrigeerd failed")
		}

		financieleZaak.Zaakkenmerk = financieleVerplichtingGecorrigeerd.Zaakkenmerk
		financieleZaak.Bsn = financieleVerplichtingGecorrigeerd.Bsn
		financieleZaak.TotaalFinancieelVerplicht += financieleVerplichtingGecorrigeerd.NieuwOpgelegdBedrag
		financieleZaak.SaldoDatumtijd = financieleVerplichtingGecorrigeerd.DatumtijdGebeurtenis

	case "BetalingsverplichtingOpgelegd":
		betalingsverplichtingOpgelegd, ok := event.(BetalingsverplichtingOpgelegd)
		if !ok {
			return fmt.Errorf("cast to BetalingsverplichtingOpgelegd failed")
		}

		financieleZaak.Zaakkenmerk = betalingsverplichtingOpgelegd.Zaakkenmerk
		financieleZaak.Bsn = betalingsverplichtingOpgelegd.Bsn

	case "BetalingsverplichtingIngetrokken":
		_, ok := event.(BetalingsverplichtingIngetrokken)
		if !ok {
			return fmt.Errorf("cast to BetalingsverplichtingIngetrokken failed")
		}

	case "BetalingVerwerkt":
		betalingVerwerkt, ok := event.(BetalingVerwerkt)
		if !ok {
			return fmt.Errorf("cast to BetalingVerwerkt failed")
		}

		financieleZaak.TotaalFinancieelVereffend += betalingVerwerkt.Bedrag
		financieleZaak.SaldoDatumtijd = betalingVerwerkt.DatumtijdGebeurtenis

	case "FinancieelRechtVastgesteld":
		financieelRechtVastgesteld, ok := event.(FinancieelRechtVastgesteld)
		if !ok {
			return fmt.Errorf("cast to FinancieelRechtVastgesteld failed")
		}

		financieleZaak.Zaakkenmerk = financieelRechtVastgesteld.Zaakkenmerk
		financieleZaak.Bsn = financieelRechtVastgesteld.Bsn
		financieleZaak.TotaalFinancieelVerplicht -= financieelRechtVastgesteld.Bedrag
		financieleZaak.SaldoDatumtijd = financieelRechtVastgesteld.DatumtijdGebeurtenis

	case "BedragUitbetaald":
		bedragUitbetaald, ok := event.(BedragUitbetaald)
		if !ok {
			return fmt.Errorf("cast to BedragUitbetaald failed")
		}

		financieleZaak.Zaakkenmerk = bedragUitbetaald.Zaakkenmerk
		financieleZaak.Bsn = bedragUitbetaald.Bsn
		financieleZaak.TotaalFinancieelVereffend -= bedragUitbetaald.Bedrag
		financieleZaak.SaldoDatumtijd = bedragUitbetaald.DatumtijdGebeurtenis

	case "VerrekeningVerwerkt":
		verrekeningVerwerkt, ok := event.(VerrekeningVerwerkt)
		if !ok {
			return fmt.Errorf("cast to VerrekeningVerwerkt failed")
		}

		financieleZaak.Zaakkenmerk = verrekeningVerwerkt.Zaakkenmerk
		financieleZaak.Bsn = verrekeningVerwerkt.Bsn
		financieleZaak.TotaalFinancieelVereffend += verrekeningVerwerkt.BedragVerrekening
		financieleZaak.SaldoDatumtijd = verrekeningVerwerkt.DatumtijdGebeurtenis

	case "FinancieleZaakOvergedragen":
		financieleZaakOvergedragen, ok := event.(FinancieleZaakOvergedragen)
		if !ok {
			return fmt.Errorf("cast to FinancieleZaakOvergedragen failed")
		}

		financieleZaak.Zaakkenmerk = financieleZaakOvergedragen.Zaakkenmerk
		financieleZaak.Bsn = financieleZaakOvergedragen.Bsn

	case "FinancieleZaakOvergenomen":
		financieleZaakOvergenomen, ok := event.(FinancieleZaakOvergenomen)
		if !ok {
			return fmt.Errorf("cast to FinancieleZaakOvergenomen failed")
		}

		financieleZaak.Zaakkenmerk = financieleZaakOvergenomen.Zaakkenmerk
		financieleZaak.Bsn = financieleZaakOvergenomen.Bsn

	case "FinancieleZaakOvergedragenAanDeurwaarder":
		financieleZaakOvergedragenAanDeurwaarder, ok := event.(FinancieleZaakOvergedragenAanDeurwaarder)
		if !ok {
			return fmt.Errorf("cast to FinancieleZaakOvergedragenAanDeurwaarder failed")
		}
		financieleZaak.Zaakkenmerk = financieleZaakOvergedragenAanDeurwaarder.Zaakkenmerk
		financieleZaak.Bsn = financieleZaakOvergedragenAanDeurwaarder.Bsn
	}

	financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

	financieleZaak.Saldo = financieleZaak.TotaalFinancieelVerplicht - financieleZaak.TotaalFinancieelVereffend

	return nil
}
