// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type BeschikbaarheidStatus string

const (
	BeschikbaarheidStatusBeschikbaar     BeschikbaarheidStatus = "BESCHIKBAAR"
	BeschikbaarheidStatusNietBeschikbaar BeschikbaarheidStatus = "NIET_BESCHIKBAAR"
	BeschikbaarheidStatusStoring         BeschikbaarheidStatus = "STORING"
)

func BeschikbaarheidStatusFromValue(value string) BeschikbaarheidStatus {
	switch value {
	case "BESCHIKBAAR":
		return BeschikbaarheidStatusBeschikbaar
	case "NIET_BESCHIKBAAR":
		return BeschikbaarheidStatusNietBeschikbaar
	default:
		return BeschikbaarheidStatusStoring
	}
}
