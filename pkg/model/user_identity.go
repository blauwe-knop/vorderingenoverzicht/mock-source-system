// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type Bsn string

type UserIdentity struct {
	Bsn Bsn `json:"bsn"`
}
