package model_test

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

func TestFinancialClaimsInformationDocumentFinancieleZaak_On(t *testing.T) {
	type args struct {
		event model.Event
	}
	tests := []struct {
		name          string
		args          args
		want          *model.FinancialClaimsInformationDocumentFinancieleZaak
		expectedError error
	}{
		{
			name: "creating financiele zaak based on FinancieleVerplichtingOpgelegd event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on FinancieleVerplichtingKwijtgescholden event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on FinancieleVerplichtingGecorrigeerd event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on BetalingsverplichtingOpgelegd event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on BetalingsverplichtingIngetrokken event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on BetalingVerwerkt event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.BetalingVerwerkt{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.BetalingVerwerkt{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on FinancieelRechtVastgesteld event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieelRechtVastgesteld{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieelRechtVastgesteld{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on BedragUitbetaald event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.BedragUitbetaald{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.BedragUitbetaald{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on VerrekeningVerwerkt event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.VerrekeningVerwerkt{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.VerrekeningVerwerkt{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on FinancieleZaakOvergedragen event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragen{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleZaakOvergedragen{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on FinancieleZaakOvergenomen event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergenomen{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleZaakOvergenomen{})),
			expectedError: nil,
		},
		{
			name: "creating financiele zaak based on FinancieleZaakOvergedragenAanDeurwaarder event results in valid financiele zaak",
			args: args{
				event: createEvent(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{})),
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{})),
			expectedError: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			financieleZaak := &model.FinancialClaimsInformationDocumentFinancieleZaak{
				Achterstanden: getAchterstanden(),
				ContactOpties: getContactOpties(),
			}
			err := financieleZaak.On(tt.args.event)
			assert.Condition(t, func() bool {
				return assertFinancieleZaakEqual(t, tt.want, financieleZaak)
			}, "The actual FinancieleZaak should be equal to expected. \nexpected: \"%v\"\nactual:   \"%v\"", tt.want, financieleZaak)
			if err != nil {
				assert.EqualErrorf(t, err, "failed to bla", "On() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestNewFinancieleZaakFromEvents(t *testing.T) {
	type args struct {
		events []model.Event
	}
	tests := []struct {
		name          string
		args          args
		want          *model.FinancialClaimsInformationDocumentFinancieleZaak
		expectedError error
	}{
		{
			name: "creating new financiele zaak based on FinancieleVerplichtingOpgelegd event, achterstanden and contactopties results in valid financiele zaak",
			args: args{
				events: []model.Event{
					createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
				},
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			expectedError: nil,
		},
		{
			name: "creating new financiele zaak based on multiple events, achterstanden and contactopties results in valid financiele zaak",
			args: args{
				events: []model.Event{
					createEvent(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{})),
					createEvent(reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
				},
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}), reflect.TypeOf(model.BetalingsverplichtingIngetrokken{})),
			expectedError: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			financieleZaak, err := model.NewFinancieleZaakFromEvents(tt.args.events, getAchterstanden(), getContactOpties())
			assert.Condition(t, func() bool {
				return assertFinancieleZaakEqual(t, tt.want, financieleZaak)
			}, "The actual FinancieleZaak should be equal to expected. \nexpected: \"%v\"\nactual:   \"%v\"", tt.want, financieleZaak)
			if err != nil {
				assert.EqualErrorf(t, err, "failed to bla", "On() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func TestJsonFinancieleZaak(t *testing.T) {
	type args struct {
		events []model.Event
	}
	tests := []struct {
		name          string
		args          args
		want          *model.FinancialClaimsInformationDocumentFinancieleZaak
		expectedError error
	}{
		{
			name: "creating new financiele zaak based on FinancieleVerplichtingOpgelegd event, achterstanden and contactopties results in valid financiele zaak",
			args: args{
				events: []model.Event{
					createEvent(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
				},
			},
			want:          getFinancieleZaak(reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{})),
			expectedError: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			financieleZaak, err := model.NewFinancieleZaakFromEvents(tt.args.events, getAchterstanden(), getContactOpties())
			assert.NoError(t, err)

			financieleZaakAsJson, err := json.Marshal(&financieleZaak)
			assert.NoError(t, err)

			var financieleZaakAfterParsing model.FinancialClaimsInformationDocumentFinancieleZaak
			err = json.Unmarshal(financieleZaakAsJson, &financieleZaakAfterParsing)
			assert.NoError(t, err)

			assert.Condition(t, func() bool {
				return assertFinancieleZaakEqual(t, financieleZaak, &financieleZaakAfterParsing)
			}, "The actual FinancieleZaak should be equal to expected. \nexpected: \"%v\"\nactual:   \"%v\"", tt.want, financieleZaak)

			assert.Condition(t, func() bool {
				return assertFinancieleZaakEqual(t, tt.want, financieleZaak)
			}, "The actual FinancieleZaak should be equal to expected. \nexpected: \"%v\"\nactual:   \"%v\"", tt.want, financieleZaak)
			if err != nil {
				assert.EqualErrorf(t, err, "failed to bla", "On() error = %v, expectedError %v", err, tt.expectedError)
			}
		})
	}
}

func getFinancieleZaak(eventTypes ...reflect.Type) *model.FinancialClaimsInformationDocumentFinancieleZaak {
	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	financieleZaak := &model.FinancialClaimsInformationDocumentFinancieleZaak{
		SaldoDatumtijd: datumtijd,
		Achterstanden:  getAchterstanden(),
		ContactOpties:  getContactOpties(),
	}

	for _, eventType := range eventTypes {
		event := createEvent(eventType)

		switch event.GetBaseEvent().GebeurtenisType {
		case reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 50
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 50
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = -50
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = -50
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 60
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 60
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 0
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}).Name():
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 0
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.BetalingVerwerkt{}).Name():
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 60
			financieleZaak.Saldo = -60
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.FinancieelRechtVastgesteld{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = -50
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = -50
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.BedragUitbetaald{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = -50
			financieleZaak.Saldo = 50
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.VerrekeningVerwerkt{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 40
			financieleZaak.Saldo = -40
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.FinancieleZaakOvergedragen{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 0
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.FinancieleZaakOvergenomen{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 0
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)

		case reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}).Name():
			financieleZaak.Zaakkenmerk = "Zaakkenmerk"
			financieleZaak.Bsn = "Bsn"
			financieleZaak.TotaalFinancieelVerplicht = 0
			financieleZaak.TotaalFinancieelVereffend = 0
			financieleZaak.Saldo = 0
			financieleZaak.Gebeurtenissen = append(financieleZaak.Gebeurtenissen, event)
		}
	}
	return financieleZaak
}

func getAchterstanden() []model.GebeurtenisKenmerk {
	return []model.GebeurtenisKenmerk{
		"GebeurtenisKenmerk",
	}
}

func getContactOpties() []model.ContactOptie {
	return []model.ContactOptie{
		{
			ContactVorm:  model.ContactVormTelefoon,
			Url:          "tel:+318000543",
			Naam:         "Belastingtelefoon",
			Omschrijving: "Voor direct contact over uw zaak",
			Prioriteit:   1,
		},
		{
			ContactVorm:  model.ContactVormEmail,
			Url:          "info@belastingdienst.nl",
			Naam:         "Belastingmail",
			Omschrijving: "Voor direct contact over uw zaak",
			Prioriteit:   2,
		},
	}
}

func createEvent(eventType reflect.Type) model.Event {
	datumtijd, _ := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")

	switch eventType.Name() {
	case reflect.TypeOf(model.FinancieleVerplichtingOpgelegd{}).Name():
		return model.FinancieleVerplichtingOpgelegd{
			DatumtijdOpgelegd:               datumtijd,
			Zaakkenmerk:                     "Zaakkenmerk",
			Beschikkingsnummer:              "Beschikkingsnummer",
			Bsn:                             "Bsn",
			PrimaireVerplichting:            true,
			Type:                            "Type",
			Categorie:                       "Categorie",
			Bedrag:                          50,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			OpgelegdDoor:                    "OpgelegdDoor",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleVerplichtingKwijtgescholden{}).Name():
		return model.FinancieleVerplichtingKwijtgescholden{
			KenmerkKwijtgescholdenFinancieleVerplichting: "",
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			BedragKwijtschelding:            50,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleVerplichtingKwijtgescholden",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleVerplichtingGecorrigeerd{}).Name():
		return model.FinancieleVerplichtingGecorrigeerd{
			KenmerkGecorrigeerdeFinancieleVerplichting: "",
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			NieuwOpgelegdBedrag:             60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleVerplichtingGecorrigeerd",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieelRechtVastgesteld{}).Name():
		return model.FinancieelRechtVastgesteld{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			Bedrag:                          50,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieelRechtVastgesteld",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BetalingsverplichtingIngetrokken{}).Name():
		return model.BetalingsverplichtingIngetrokken{
			IngetrokkenGebeurtenisKenmerk: "IngetrokkenGebeurtenisKenmerk",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BetalingsverplichtingIngetrokken",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BetalingsverplichtingOpgelegd{}).Name():
		return model.BetalingsverplichtingOpgelegd{
			DatumtijdOpgelegd:            datumtijd,
			Zaakkenmerk:                  "Zaakkenmerk",
			Bsn:                          "Bsn",
			Bedrag:                       50,
			Omschrijving:                 "Omschrijving",
			Type:                         "Type",
			Betaalwijze:                  "Betaalwijze",
			TeBetalenAan:                 "TeBetalenAan",
			Rekeningnummer:               "Rekeningnummer",
			RekeningnummerTenaamstelling: "RekeningnummerTenaamstelling",
			Betalingskenmerk:             "Betalingskenmerk",
			Vervaldatum:                  datumtijd,
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BetalingsverplichtingOpgelegd",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BetalingVerwerkt{}).Name():
		return model.BetalingVerwerkt{
			DatumtijdVerwerkt:  datumtijd,
			Betalingskenmerk:   "Betalingskenmerk",
			DatumtijdOntvangen: datumtijd,
			OntvangenDoor:      "OntvangenDoor",
			VerwerktDoor:       "VerwerktDoor",
			Bedrag:             60,
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BetalingVerwerkt",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.BedragUitbetaald{}).Name():
		return model.BedragUitbetaald{
			Zaakkenmerk:    "Zaakkenmerk",
			Bsn:            "Bsn",
			Bedrag:         50,
			Rekeningnummer: "Rekeningnummer",
			Omschrijving:   "Omschrijving",
			UitgevoerdDoor: "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "BedragUitbetaald",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.VerrekeningVerwerkt{}).Name():
		return model.VerrekeningVerwerkt{
			KenmerkFinancieleVerplichting:   "",
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			BedragVerrekening:               40,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "VerrekeningVerwerkt",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleZaakOvergedragen{}).Name():
		return model.FinancieleZaakOvergedragen{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			SaldoBijOverdracht:              60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			OvergedragenAan:                 "OvergedragenAan",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleZaakOvergedragen",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleZaakOvergenomen{}).Name():
		return model.FinancieleZaakOvergenomen{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			SaldoBijOverdracht:              60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			OvergenomenVan:                  "OvergenomenVan",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleZaakOvergenomen",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}

	case reflect.TypeOf(model.FinancieleZaakOvergedragenAanDeurwaarder{}).Name():
		return model.FinancieleZaakOvergedragenAanDeurwaarder{
			Zaakkenmerk:                     "Zaakkenmerk",
			Bsn:                             "Bsn",
			SaldoBijOverdracht:              60,
			Omschrijving:                    "Omschrijving",
			JuridischeGrondslagOmschrijving: "JuridischeGrondslagOmschrijving",
			JuridischeGrondslagBron:         "JuridischeGrondslagBron",
			UitgevoerdDoor:                  "UitgevoerdDoor",
			OvergedragenAan:                 "OvergedragenAan",
			NaamDeurwaarder:                 "NaamDeurwaarder",
			TelefoonnummerDeurwaarderUrl:    "TelefoonnummerDeurwaarderUrl",
			EmailAdresDeurwaarderUrl:        "EmailAdresDeurwaarderUrl",
			BaseEvent: model.BaseEvent{
				GebeurtenisType:      "FinancieleZaakOvergedragenAanDeurwaarder",
				GebeurtenisKenmerk:   "GebeurtenisKenmerk",
				DatumtijdGebeurtenis: datumtijd,
			},
		}
	}

	return nil
}

func assertFinancieleZaakEqual(t testing.TB, expected, actual *model.FinancialClaimsInformationDocumentFinancieleZaak) bool {
	if expected == nil && actual == nil {
		return true
	} else if expected == nil || actual == nil {
		return false
	}

	if actual.Bsn != expected.Bsn ||
		actual.Zaakkenmerk != expected.Zaakkenmerk ||
		actual.Saldo != expected.Saldo ||
		actual.TotaalFinancieelVerplicht != expected.TotaalFinancieelVerplicht ||
		actual.TotaalFinancieelVereffend != expected.TotaalFinancieelVereffend {
		return false
	}
	for ig, actualGebeurtenis := range actual.Gebeurtenissen {
		expectedGebeurtenis := expected.Gebeurtenissen[ig]
		switch ag := actualGebeurtenis.(type) {
		case model.FinancieleVerplichtingOpgelegd:
			switch eg := expectedGebeurtenis.(type) {
			case model.FinancieleVerplichtingOpgelegd:
				if ag.Bsn != eg.Bsn ||
					ag.Zaakkenmerk != eg.Zaakkenmerk ||
					ag.Type != eg.Type ||
					ag.UitgevoerdDoor != eg.UitgevoerdDoor ||
					ag.JuridischeGrondslagOmschrijving != eg.JuridischeGrondslagOmschrijving ||
					ag.JuridischeGrondslagBron != eg.JuridischeGrondslagBron ||
					ag.Omschrijving != eg.Omschrijving ||
					ag.Beschikkingsnummer != eg.Beschikkingsnummer ||
					ag.Categorie != eg.Categorie ||
					ag.GebeurtenisType != eg.GebeurtenisType ||
					ag.GebeurtenisKenmerk != eg.GebeurtenisKenmerk ||
					ag.OpgelegdDoor != eg.OpgelegdDoor ||
					ag.Bedrag != eg.Bedrag ||
					ag.PrimaireVerplichting != eg.PrimaireVerplichting {
					return false
				}
			default:
				return false
			}
		case model.BetalingsverplichtingOpgelegd:
			switch eg := expectedGebeurtenis.(type) {
			case model.BetalingsverplichtingOpgelegd:
				if ag.Bsn != eg.Bsn ||
					ag.Zaakkenmerk != eg.Zaakkenmerk ||
					ag.Type != eg.Type ||
					ag.Omschrijving != eg.Omschrijving ||
					ag.GebeurtenisType != eg.GebeurtenisType ||
					ag.GebeurtenisKenmerk != eg.GebeurtenisKenmerk ||
					ag.Betalingskenmerk != eg.Betalingskenmerk ||
					ag.Betaalwijze != eg.Betaalwijze ||
					ag.RekeningnummerTenaamstelling != eg.RekeningnummerTenaamstelling ||
					ag.Rekeningnummer != eg.Rekeningnummer ||
					ag.TeBetalenAan != eg.TeBetalenAan ||
					ag.Bedrag != eg.Bedrag {
					return false
				}
			default:
				return false
			}
		}
	}
	for ia, actualAchterstand := range actual.Achterstanden {
		expectedAchterstand := expected.Achterstanden[ia]
		if actualAchterstand != expectedAchterstand {
			return false
		}
	}
	for ic, actualContactOpties := range actual.ContactOpties {
		expectedContactOpties := expected.ContactOpties[ic]
		if actualContactOpties != expectedContactOpties {
			return false
		}
	}

	return true
}
