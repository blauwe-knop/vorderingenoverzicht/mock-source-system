// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import "time"

type Event interface {
	IsEvent()
	GetBaseEvent() BaseEvent
}

type GebeurtenisKenmerk string
type Zaakkenmerk string

type BaseEvent struct {
	GebeurtenisType      string             `json:"gebeurtenis_type"`
	GebeurtenisKenmerk   GebeurtenisKenmerk `json:"gebeurtenis_kenmerk"`
	DatumtijdGebeurtenis time.Time          `json:"datumtijd_gebeurtenis"`
}

func (e FinancieleVerplichtingOpgelegd) IsEvent() {}
func (e FinancieleVerplichtingOpgelegd) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e FinancieleVerplichtingKwijtgescholden) IsEvent() {}
func (e FinancieleVerplichtingKwijtgescholden) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e FinancieleVerplichtingGecorrigeerd) IsEvent() {}
func (e FinancieleVerplichtingGecorrigeerd) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e BetalingsverplichtingOpgelegd) IsEvent() {}
func (e BetalingsverplichtingOpgelegd) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e BetalingsverplichtingIngetrokken) IsEvent() {}
func (e BetalingsverplichtingIngetrokken) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e BetalingVerwerkt) IsEvent() {}
func (e BetalingVerwerkt) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e FinancieelRechtVastgesteld) IsEvent() {}
func (e FinancieelRechtVastgesteld) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e BedragUitbetaald) IsEvent() {}
func (e BedragUitbetaald) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e VerrekeningVerwerkt) IsEvent() {}
func (e VerrekeningVerwerkt) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e FinancieleZaakOvergedragen) IsEvent() {}
func (e FinancieleZaakOvergedragen) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e FinancieleZaakOvergenomen) IsEvent() {}
func (e FinancieleZaakOvergenomen) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

func (e FinancieleZaakOvergedragenAanDeurwaarder) IsEvent() {}
func (e FinancieleZaakOvergedragenAanDeurwaarder) GetBaseEvent() BaseEvent {
	return e.BaseEvent
}

type FinancieleVerplichtingOpgelegd struct {
	DatumtijdOpgelegd               time.Time                  `json:"datumtijd_opgelegd"`
	Zaakkenmerk                     Zaakkenmerk                `json:"zaakkenmerk"`
	Beschikkingsnummer              string                     `json:"beschikkingsnummer,omitempty"`
	Bsn                             Bsn                        `json:"bsn"`
	PrimaireVerplichting            bool                       `json:"primaire_verplichting"`
	Type                            FinancieleVerplichtingType `json:"type"`
	Categorie                       string                     `json:"categorie"`
	Bedrag                          int                        `json:"bedrag"`
	Omschrijving                    string                     `json:"omschrijving"`
	JuridischeGrondslagOmschrijving string                     `json:"juridische_grondslag_omschrijving"`
	JuridischeGrondslagBron         string                     `json:"juridische_grondslag_bron,omitempty"`
	OpgelegdDoor                    string                     `json:"opgelegd_door"`
	UitgevoerdDoor                  string                     `json:"uitgevoerd_door"`
	BaseEvent
}

type FinancieleVerplichtingKwijtgescholden struct {
	KenmerkKwijtgescholdenFinancieleVerplichting string      `json:"kenmerk_kwijtgescholden_financiele_verplichting"`
	Zaakkenmerk                                  Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                                          Bsn         `json:"bsn"`
	BedragKwijtschelding                         int         `json:"bedrag_kwijtschelding"`
	Omschrijving                                 string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving              string      `json:"juridische_grondslag_omschrijving"`
	JuridischeGrondslagBron                      string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                               string      `json:"uitgevoerd_door"`
	BaseEvent
}

type FinancieleVerplichtingGecorrigeerd struct {
	KenmerkGecorrigeerdeFinancieleVerplichting string      `json:"kenmerk_gecorrigeerde_financiele_verplichting"`
	Zaakkenmerk                                Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                                        Bsn         `json:"bsn"`
	NieuwOpgelegdBedrag                        int         `json:"nieuw_opgelegd_bedrag"`
	Omschrijving                               string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving            string      `json:"juridische_grondslag_omschrijving,omitempty"`
	JuridischeGrondslagBron                    string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                             string      `json:"uitgevoerd_door"`
	BaseEvent
}

type BetalingsverplichtingOpgelegd struct {
	DatumtijdOpgelegd            time.Time   `json:"datumtijd_opgelegd"`
	Zaakkenmerk                  Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                          Bsn         `json:"bsn"`
	Bedrag                       int         `json:"bedrag"`
	Omschrijving                 string      `json:"omschrijving,omitempty"`
	Type                         string      `json:"type"`
	Betaalwijze                  string      `json:"betaalwijze"`
	TeBetalenAan                 string      `json:"te_betalen_aan"`
	Rekeningnummer               string      `json:"rekeningnummer,omitempty"`
	RekeningnummerTenaamstelling string      `json:"rekeningnummer_tenaamstelling,omitempty"`
	Betalingskenmerk             string      `json:"betalingskenmerk,omitempty"`
	Vervaldatum                  time.Time   `json:"vervaldatum"`
	BaseEvent
}

type BetalingsverplichtingIngetrokken struct {
	IngetrokkenGebeurtenisKenmerk GebeurtenisKenmerk `json:"ingetrokken_gebeurtenis_kenmerk"`
	BaseEvent
}

type BetalingVerwerkt struct {
	DatumtijdVerwerkt  time.Time `json:"datumtijd_verwerkt"`
	Betalingskenmerk   string    `json:"betalingskenmerk"`
	DatumtijdOntvangen time.Time `json:"datumtijd_ontvangen,omitempty"`
	OntvangenDoor      string    `json:"ontvangen_door"`
	VerwerktDoor       string    `json:"verwerkt_door"`
	Bedrag             int       `json:"bedrag"`
	BaseEvent
}

type FinancieelRechtVastgesteld struct {
	Zaakkenmerk                     Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                             Bsn         `json:"bsn"`
	Bedrag                          int         `json:"bedrag"`
	Omschrijving                    string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving string      `json:"juridische_grondslag_omschrijving"`
	JuridischeGrondslagBron         string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                  string      `json:"uitgevoerd_door"`
	BaseEvent
}

type BedragUitbetaald struct {
	Zaakkenmerk    Zaakkenmerk `json:"zaakkenmerk"`
	Bsn            Bsn         `json:"bsn"`
	Bedrag         int         `json:"bedrag"`
	Rekeningnummer string      `json:"rekeningnummer"`
	Omschrijving   string      `json:"omschrijving"`
	UitgevoerdDoor string      `json:"uitgevoerd_door"`
	BaseEvent
}

type VerrekeningVerwerkt struct {
	KenmerkFinancieleVerplichting   string      `json:"kenmerk_financiele_verplichting"`
	Zaakkenmerk                     Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                             Bsn         `json:"bsn"`
	BedragVerrekening               int         `json:"bedrag_verrekening"`
	Omschrijving                    string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving string      `json:"juridische_grondslag_omschrijving"`
	JuridischeGrondslagBron         string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                  string      `json:"uitgevoerd_door"`
	BaseEvent
}

type FinancieleZaakOvergedragen struct {
	Zaakkenmerk                     Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                             Bsn         `json:"bsn"`
	SaldoBijOverdracht              int         `json:"saldo_bij_overdracht"`
	Omschrijving                    string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving string      `json:"juridische_grondslag_omschrijving,omitempty"`
	JuridischeGrondslagBron         string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                  string      `json:"uitgevoerd_door"`
	OvergedragenAan                 string      `json:"overgedragen_aan"`
	BaseEvent
}

type FinancieleZaakOvergenomen struct {
	Zaakkenmerk                     Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                             Bsn         `json:"bsn"`
	SaldoBijOverdracht              int         `json:"saldo_bij_overdracht"`
	Omschrijving                    string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving string      `json:"juridische_grondslag_omschrijving,omitempty"`
	JuridischeGrondslagBron         string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                  string      `json:"uitgevoerd_door"`
	OvergenomenVan                  string      `json:"overgenomen_van"`
	BaseEvent
}

type FinancieleZaakOvergedragenAanDeurwaarder struct {
	Zaakkenmerk                     Zaakkenmerk `json:"zaakkenmerk"`
	Bsn                             Bsn         `json:"bsn"`
	SaldoBijOverdracht              int         `json:"saldo_bij_overdracht"`
	Omschrijving                    string      `json:"omschrijving"`
	JuridischeGrondslagOmschrijving string      `json:"juridische_grondslag_omschrijving,omitempty"`
	JuridischeGrondslagBron         string      `json:"juridische_grondslag_bron,omitempty"`
	UitgevoerdDoor                  string      `json:"uitgevoerd_door"`
	OvergedragenAan                 string      `json:"overgedragen_aan"`
	NaamDeurwaarder                 string      `json:"naam_deurwaarder,omitempty"`
	TelefoonnummerDeurwaarderUrl    string      `json:"telefoonnummer_deurwaarder_url,omitempty"`
	EmailAdresDeurwaarderUrl        string      `json:"email_adres_deurwaarder_url,omitempty"`
	BaseEvent
}
