// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

type ContactVorm string

const (
	ContactVormTelefoon            ContactVorm = "telefoon"
	ContactVormEmail               ContactVorm = "e_mail"
	ContactVormPersoonlijkeWebsite ContactVorm = "persoonlijke_website"
	ContactVormPubliekeWebsite     ContactVorm = "publieke_website"
	ContactVormOnbekend            ContactVorm = "onbekend"
)

func ContactVormFromValue(value string) ContactVorm {
	switch value {
	case "telefoon":
		return ContactVormTelefoon
	case "e_mail":
		return ContactVormEmail
	case "persoonlijke_website":
		return ContactVormPersoonlijkeWebsite
	case "publieke_website":
		return ContactVormPubliekeWebsite
	default:
		return ContactVormOnbekend
	}
}
