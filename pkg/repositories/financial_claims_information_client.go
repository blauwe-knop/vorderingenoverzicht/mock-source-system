// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

type FinancialClaimsInformationClient struct {
	baseURL string
	log     *zap.Logger
}

var ErrFinancialClaimsInformationNotFound = errors.New("financial claims information does not exist")

func NewFinancialClaimsInformationClient(baseURL string, log *zap.Logger) *FinancialClaimsInformationClient {
	return &FinancialClaimsInformationClient{
		baseURL: baseURL,
		log:     log,
	}
}

func (s *FinancialClaimsInformationClient) Get(id model.UserIdentity) (*model.FinancialClaimsInformationDocument, error) {
	url := fmt.Sprintf("%s/financial_claims_information", s.baseURL)

	requestBodyAsJson, err := json.Marshal(id)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	// Note: post instead of get as bsn is being send with this request.
	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create post financial claims information request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get financial claims information: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrFinancialClaimsInformationNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving financial claims information: %d, id: %s", resp.StatusCode, id)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	financialClaimsInformationDocument := model.FinancialClaimsInformationDocument{}
	err = json.Unmarshal(body, &financialClaimsInformationDocument)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, id: %s: %v", id, err)
	}

	return &financialClaimsInformationDocument, nil
}

func (s *FinancialClaimsInformationClient) GetHealthCheck() healthcheck.Result {
	name := "mock-source-system"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		s.log.Log(zap.ErrorLevel, "failed to create health check request", zap.Error(err))
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.Do(request)
	if err != nil {
		s.log.Log(zap.ErrorLevel, "failed to get health check request", zap.Error(err))
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			s.log.Log(zap.ErrorLevel, "failed to parse body health check request", zap.Error(err))
		} else {
			s.log.Log(zap.ErrorLevel, "failed to parse body health check request", zap.Int("statuscode", response.StatusCode), zap.ByteString("body", body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		s.log.Log(zap.ErrorLevel, "Unmarshal error", zap.Error(err))
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
