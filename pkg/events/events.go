package events

var (
	MSS_1 = NewEvent(
		"mss_1",
		"started listening",
		Low,
	)
	MSS_2 = NewEvent(
		"mss_2",
		"server closed",
		High,
	)
	MSS_3 = NewEvent(
		"mss_3",
		"received request for financial claims information document",
		Low,
	)
	MSS_4 = NewEvent(
		"mss_4",
		"failed to decode request payload",
		High,
	)
	MSS_5 = NewEvent(
		"mss_5",
		"failed to parse time for document datum tijd",
		High,
	)
	MSS_6 = NewEvent(
		"mss_6",
		"failed to parse time for saldo datum tijd",
		High,
	)
	MSS_7 = NewEvent(
		"mss_7",
		"failed to parse time for datum tijd opgelegd",
		High,
	)
	MSS_8 = NewEvent(
		"mss_8",
		"failed to parse time for datum tijd kwijtgescholden",
		High,
	)
	MSS_9 = NewEvent(
		"mss_9",
		"failed to parse time for datum tijd gecorrigeerd",
		High,
	)
	MSS_10 = NewEvent(
		"mss_10",
		"failed to parse time for datum tijd ingetrokken",
		High,
	)
	MSS_11 = NewEvent(
		"mss_11",
		"failed to parse time for datum tijd betaling verwerkt",
		High,
	)
	MSS_12 = NewEvent(
		"mss_12",
		"failed to parse time for datum tijd betaling ontvangen",
		High,
	)
	MSS_13 = NewEvent(
		"mss_13",
		"failed to parse time for datum tijd financieel recht vastgesteld",
		High,
	)
	MSS_14 = NewEvent(
		"mss_14",
		"failed to parse time for datum tijd bedrag uitbetaald",
		High,
	)
	MSS_15 = NewEvent(
		"mss_15",
		"failed to parse time for datum tijd verrekening verwerkt",
		High,
	)
	MSS_16 = NewEvent(
		"mss_16",
		"failed to parse time for datum tijd financiele zaak overgedragen",
		High,
	)
	MSS_17 = NewEvent(
		"mss_17",
		"failed to parse time for datum tijd financiele zaak overgenomen",
		High,
	)
	MSS_18 = NewEvent(
		"mss_18",
		"failed to parse time for datum tijd financiele zaak overgedragen aan deurwaarder",
		High,
	)
	MSS_19 = NewEvent(
		"mss_19",
		"failed to encode response payload",
		High,
	)
	MSS_20 = NewEvent(
		"mss_20",
		"sent response for financial claims information document",
		High,
	)
	MSS_21 = NewEvent(
		"mss_21",
		"received request for openapi spec as JSON",
		Low,
	)
	MSS_22 = NewEvent(
		"mss_22",
		"failed to read openapi.json file",
		High,
	)
	MSS_23 = NewEvent(
		"mss_23",
		"failed to write fileBytes",
		High,
	)
	MSS_24 = NewEvent(
		"mss_24",
		"sent response with openapi spec as JSON",
		Low,
	)
	MSS_25 = NewEvent(
		"mss_25",
		"received request for openapi spec as YAML",
		Low,
	)
	MSS_26 = NewEvent(
		"mss_26",
		"failed to read openapi.yaml file",
		High,
	)
	MSS_27 = NewEvent(
		"mss_27",
		"failed to write fileBytes",
		High,
	)
	MSS_28 = NewEvent(
		"mss_28",
		"sent response with openapi spec as YAML",
		Low,
	)
)
