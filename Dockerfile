FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/mock-source-system/api
COPY ./cmd /go/src/mock-source-system/cmd
COPY ./internal /go/src/mock-source-system/internal
COPY ./pkg /go/src/mock-source-system/pkg
COPY ./go.mod /go/src/mock-source-system/
COPY ./go.sum /go/src/mock-source-system/
WORKDIR /go/src/mock-source-system
RUN go mod download \
 && go build -o dist/bin/mock-source-system ./cmd/mock-source-system

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/mock-source-system/dist/bin/mock-source-system /usr/local/bin/mock-source-system
COPY --from=build /go/src/mock-source-system/api/openapi.json /api/openapi.json
COPY --from=build /go/src/mock-source-system/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/mock-source-system"]
