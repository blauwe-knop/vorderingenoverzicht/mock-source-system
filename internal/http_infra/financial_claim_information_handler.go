// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"net/http"
	"time"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/model"
)

func handlerGetFinancialClaimsInformationDocument(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.MSS_3
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var userIdentity model.UserIdentity
	err := json.NewDecoder(request.Body).Decode(&userIdentity)
	if err != nil {
		event = events.MSS_4
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	documentDatumtijd, err := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")
	if err != nil {
		event = events.MSS_5
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	saldoDatumtijd, err := time.Parse(time.RFC3339, "2022-07-13T18:43:12+00:00")
	if err != nil {
		event = events.MSS_6
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdOpgelegd, err := time.Parse(time.RFC3339, "2022-07-12T00:00:00+00:00")
	if err != nil {
		event = events.MSS_7
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdKwijtgescholden, err := time.Parse(time.RFC3339, "2022-07-12T00:00:02+00:00")
	if err != nil {
		event = events.MSS_8
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdGecorrigeerd, err := time.Parse(time.RFC3339, "2022-07-12T00:00:01+00:00")
	if err != nil {
		event = events.MSS_9
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdIngetrokken, err := time.Parse(time.RFC3339, "2022-07-12T00:00:02+00:00")
	if err != nil {
		event = events.MSS_10
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdBetalingVerwerkt, err := time.Parse(time.RFC3339, "2022-07-21T00:00:00+00:00")
	if err != nil {
		event = events.MSS_11
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdBetalingOntvangen, err := time.Parse(time.RFC3339, "2022-07-20T00:00:00+00:00")
	if err != nil {
		event = events.MSS_12
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdFinancieelRechtVastgesteld, err := time.Parse(time.RFC3339, "2022-07-21T00:00:00+00:00")
	if err != nil {
		event = events.MSS_13
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdBedragUitbetaald, err := time.Parse(time.RFC3339, "2022-07-21T00:00:04+00:00")
	if err != nil {
		event = events.MSS_14
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdVerrekeningVerwerkt, err := time.Parse(time.RFC3339, "2022-07-21T00:00:04+00:00")
	if err != nil {
		event = events.MSS_15
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdFinancieleZaakOvergedragen, err := time.Parse(time.RFC3339, "2022-07-21T00:00:05+00:00")
	if err != nil {
		event = events.MSS_16
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdFinancieleZaakOvergenomen, err := time.Parse(time.RFC3339, "2022-07-21T00:00:05+00:00")
	if err != nil {
		event = events.MSS_17
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	datumtijdFinancieleZaakOvergedragenAanDeurwaarder, err := time.Parse(time.RFC3339, "2022-07-21T00:00:06+00:00")
	if err != nil {
		event = events.MSS_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	financialClaimsInformationDocument := &model.FinancialClaimsInformationDocument{
		Type:    "FINANCIAL_CLAIMS_INFORMATION_DOCUMENT",
		Version: "2",
		Body: model.FinancialClaimsInformationDocumentBody{
			AangeleverdDoor:   "00000004000000044000",
			DocumentDatumtijd: documentDatumtijd,
			Bsn:               userIdentity.Bsn,
			Beschikbaarheid: map[model.FinancieleVerplichtingType]model.BeschikbaarheidStatus{
				"WAHV_BESCHIKKINGEN":      model.BeschikbaarheidStatusFromValue("BESCHIKBAAR"),
				"SCHIKKINGEN":             model.BeschikbaarheidStatusFromValue("NIET_BESCHIKBAAR"),
				"CONFISCATIEBESLISSINGEN": model.BeschikbaarheidStatusFromValue("STORING"),
			},
			FinancieleZaken: []model.FinancialClaimsInformationDocumentFinancieleZaak{
				{
					Zaakkenmerk:               "81029991234567891",
					TotaalFinancieelVerplicht: 5500,
					TotaalFinancieelVereffend: 2000,
					Saldo:                     3500,
					SaldoDatumtijd:            saldoDatumtijd,
					Gebeurtenissen: []interface{}{
						model.FinancieleVerplichtingOpgelegd{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
								GebeurtenisKenmerk:   "81029991234567891-1",
								DatumtijdGebeurtenis: datumtijdOpgelegd,
							},
							DatumtijdOpgelegd:               datumtijdOpgelegd,
							Zaakkenmerk:                     "81029991234567891",
							Beschikkingsnummer:              "81029991234567891-123",
							Bsn:                             userIdentity.Bsn,
							PrimaireVerplichting:            true,
							Type:                            "CJIB_WAHV",
							Categorie:                       "Algemeen",
							Bedrag:                          4600,
							Omschrijving:                    "Verkeersboete",
							JuridischeGrondslagOmschrijving: "Boete voor te hard rijden Artikel 21 (RVV 1990)",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
							OpgelegdDoor:                    "00000004000000044000",
							UitgevoerdDoor:                  "00000004000000044000",
						},
						model.FinancieleVerplichtingOpgelegd{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleVerplichtingOpgelegd",
								GebeurtenisKenmerk:   "81029991234567891-2",
								DatumtijdGebeurtenis: datumtijdOpgelegd,
							},
							DatumtijdOpgelegd:               datumtijdOpgelegd,
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             userIdentity.Bsn,
							PrimaireVerplichting:            false,
							Type:                            "CJIB_ADMINISTRATIEKOSTEN",
							Categorie:                       "Administratiekosten",
							Bedrag:                          900,
							Omschrijving:                    "Administratiekosten",
							JuridischeGrondslagOmschrijving: "CJIB mag op grond van Artikel 63d van het Organisatiebesluit Ministerie van Justitie en Veiligheid administratiekosten in rekening brengen",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/BWBR0045971/2021-12-03/0",
							OpgelegdDoor:                    "00000004000000044000",
							UitgevoerdDoor:                  "00000004000000044000",
						},
						model.FinancieleVerplichtingKwijtgescholden{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleVerplichtingKwijtgescholden",
								GebeurtenisKenmerk:   "81029991234567891-3",
								DatumtijdGebeurtenis: datumtijdKwijtgescholden,
							},
							KenmerkKwijtgescholdenFinancieleVerplichting: "81029991234567891-1",
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							BedragKwijtschelding:            600,
							Omschrijving:                    "Kwijtgescholden n.a.v. onvoldoende inkomen",
							JuridischeGrondslagOmschrijving: "Recht op kwijtschelding",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
							UitgevoerdDoor:                  "00000004000000044000",
						},
						model.FinancieleVerplichtingGecorrigeerd{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleVerplichtingGecorrigeerd",
								GebeurtenisKenmerk:   "81029991234567891-4",
								DatumtijdGebeurtenis: datumtijdGecorrigeerd,
							},
							KenmerkGecorrigeerdeFinancieleVerplichting: "81029991234567891-1",
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							NieuwOpgelegdBedrag:             500,
							Omschrijving:                    "Correctie n.a.v. nieuwe telling",
							JuridischeGrondslagOmschrijving: "Definitieve vaststelling aantal",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
							UitgevoerdDoor:                  "00000004000000044000",
						},
						model.BetalingsverplichtingOpgelegd{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "BetalingsverplichtingOpgelegd",
								GebeurtenisKenmerk:   "81029991234567891-5",
								DatumtijdGebeurtenis: datumtijdOpgelegd,
							},
							DatumtijdOpgelegd:            datumtijdOpgelegd,
							Zaakkenmerk:                  "81029991234567891",
							Bsn:                          userIdentity.Bsn,
							Bedrag:                       3500,
							Omschrijving:                 "Termijn bedrag augustus",
							Type:                         "Aanmaning",
							Betaalwijze:                  "Handmatig",
							TeBetalenAan:                 "00000004000000044000",
							Rekeningnummer:               "GB33BUKB20201555555555",
							RekeningnummerTenaamstelling: "CJIB",
							Betalingskenmerk:             "81029991234567891",
							Vervaldatum:                  datumtijdOpgelegd.AddDate(0, 2, 1),
						},
						model.BetalingsverplichtingIngetrokken{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "BetalingsverplichtingIngetrokken",
								GebeurtenisKenmerk:   "81029991234567891-5",
								DatumtijdGebeurtenis: datumtijdIngetrokken,
							},
							IngetrokkenGebeurtenisKenmerk: "81029991234567891-3",
						},
						model.BetalingVerwerkt{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "BetalingVerwerkt",
								GebeurtenisKenmerk:   "81029991234567891-6",
								DatumtijdGebeurtenis: datumtijdBetalingVerwerkt,
							},
							DatumtijdVerwerkt:  datumtijdBetalingVerwerkt,
							Betalingskenmerk:   "81029991234567891",
							DatumtijdOntvangen: datumtijdBetalingOntvangen,
							OntvangenDoor:      "00000004000000044000",
							VerwerktDoor:       "00000004000000044000",
							Bedrag:             2000,
						},
						model.FinancieelRechtVastgesteld{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieelRechtVastgesteld",
								GebeurtenisKenmerk:   "81029991234567891-7",
								DatumtijdGebeurtenis: datumtijdFinancieelRechtVastgesteld,
							},
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							Bedrag:                          500,
							Omschrijving:                    "Vergoeding bij positieve uitkomst medische keuring",
							JuridischeGrondslagOmschrijving: "Recht op vergoeding bij positieve uitkomst medische keuring",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0022177&artikel=4&z=2007-10-12&g=2007-10-12",
							UitgevoerdDoor:                  "00000004000000044000",
						},
						model.BedragUitbetaald{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "BedragUitbetaald",
								GebeurtenisKenmerk:   "81029991234567891-8",
								DatumtijdGebeurtenis: datumtijdBedragUitbetaald,
							},
							Zaakkenmerk:    "81029991234567891",
							Bsn:            "814859094",
							Bedrag:         500,
							Rekeningnummer: "GB33BUKB20201555555555",
							Omschrijving:   "Uitbetaald n.a.v. positieve uitkomst medische keuring",
							UitgevoerdDoor: "00000004000000044000",
						},
						model.VerrekeningVerwerkt{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "VerrekeningVerwerkt",
								GebeurtenisKenmerk:   "81029991234567891-9",
								DatumtijdGebeurtenis: datumtijdVerrekeningVerwerkt,
							},
							KenmerkFinancieleVerplichting:   "81029991234567891-1",
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							BedragVerrekening:               600,
							Omschrijving:                    "Verrekening met Europese subsidie.",
							JuridischeGrondslagOmschrijving: "Verrekening met Europese subsidie.",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0004825&hoofdstuk=II&paragraaf=8&artikel=21&z=2023-01-01&g=2023-01-01",
							UitgevoerdDoor:                  "00000004000000044000",
						},
						model.FinancieleZaakOvergedragen{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleZaakOvergedragen",
								GebeurtenisKenmerk:   "81029991234567891-10",
								DatumtijdGebeurtenis: datumtijdFinancieleZaakOvergedragen,
							},
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							SaldoBijOverdracht:              3900,
							Omschrijving:                    "Deze zaak is overgedragen aan het CJIB.",
							JuridischeGrondslagOmschrijving: "Recht tot overdracht van uitvoering",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0022177&artikel=4&z=2007-10-12&g=2007-10-12",
							UitgevoerdDoor:                  "00000004000000044000",
							OvergedragenAan:                 "00000006000000066000",
						},
						model.FinancieleZaakOvergenomen{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleZaakOvergenomen",
								GebeurtenisKenmerk:   "81029991234567891-11",
								DatumtijdGebeurtenis: datumtijdFinancieleZaakOvergenomen,
							},
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							SaldoBijOverdracht:              3900,
							Omschrijving:                    "Zaak overgenomen van RVO",
							JuridischeGrondslagOmschrijving: "Recht tot overdracht van uitvoering",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0022177&artikel=4&z=2007-10-12&g=2007-10-12",
							UitgevoerdDoor:                  "00000006000000066000",
							OvergenomenVan:                  "00000004000000044000",
						},
						model.FinancieleZaakOvergedragenAanDeurwaarder{
							BaseEvent: model.BaseEvent{
								GebeurtenisType:      "FinancieleZaakOvergedragenAanDeurwaarder",
								GebeurtenisKenmerk:   "81029991234567891-12",
								DatumtijdGebeurtenis: datumtijdFinancieleZaakOvergedragenAanDeurwaarder,
							},
							Zaakkenmerk:                     "81029991234567891",
							Bsn:                             "814859094",
							SaldoBijOverdracht:              3900,
							Omschrijving:                    "Deze zaak is overgedragen aan de deurwaarder.",
							JuridischeGrondslagOmschrijving: "Recht tot overdracht van uitvoering",
							JuridischeGrondslagBron:         "https://wetten.overheid.nl/jci1.3:c:BWBR0022177&artikel=4&z=2007-10-12&g=2007-10-12",
							UitgevoerdDoor:                  "00000004000000044000",
							OvergedragenAan:                 "t.b.d.",
							NaamDeurwaarder:                 "Deurwaarder NL BV",
							TelefoonnummerDeurwaarderUrl:    "tel:+31581111111",
							EmailAdresDeurwaarderUrl:        "mailto:info@deurwaarderbv.nl",
						},
					},
					Achterstanden: []model.GebeurtenisKenmerk{
						"81029991234567891-3",
					},
					ContactOpties: []model.ContactOptie{
						{
							ContactVorm:  model.ContactVormEmail,
							Url:          "tel:+318000543",
							Naam:         "Belastingtelefoon",
							Omschrijving: "Voor direct contact over uw zaak",
							Prioriteit:   1,
						},
					},
				},
			},
		},
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*financialClaimsInformationDocument)
	if err != nil {
		event = events.MSS_19
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.MSS_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

}
