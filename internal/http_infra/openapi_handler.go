// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"net/http"
	"os"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system/pkg/events"
)

func handlerJson(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.MSS_21
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	fileBytes, err := os.ReadFile("/api/openapi.json")
	if err != nil {
		event = events.MSS_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		panic(err)
	}
	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/json")
	_, err = responseWriter.Write(fileBytes)

	if err != nil {
		event = events.MSS_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.MSS_24
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerYaml(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)

	event := events.MSS_25
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	fileBytes, err := os.ReadFile("/api/openapi.yaml")
	if err != nil {
		event = events.MSS_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		panic(err)
	}
	responseWriter.WriteHeader(http.StatusOK)
	responseWriter.Header().Set("Content-Type", "application/octet-stream")

	_, err = responseWriter.Write(fileBytes)
	if err != nil {
		event = events.MSS_27
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	event = events.MSS_28
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
